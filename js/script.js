//            Нарисовать на странице круг используя параметры, которые введет пользователь.

//---------------------------------------Технические требования:-------------------------------------------------
// 1. - При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг".
// Данная кнопка должна являться единственным контентом в теле HTML документа,
// весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript.
// 2. - При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга.
// При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10)
// случайного цвета. При клике на конкретный круг - этот круг должен исчезать,
// при этом пустое место заполняться, то есть все остальные круги сдвигаются влево.
// 3. - У вас может возникнуть желание поставить обработчик события на каждый круг
// для его исчезновения. Это неэффективно, так делать не нужно. На всю страницу должен
// быть только один обработчик событий, который будет это делать.

window.onload = () => {

	const divResult = document.querySelector('.result');
	const circleButton = document.getElementById('circleButton');

	const buttonPaint = document.createElement('input');
	buttonPaint.setAttribute('type', 'button');
	buttonPaint.setAttribute('value', 'Намалювати');
	buttonPaint.setAttribute('id', 'buttonPaint');

	const input = document.createElement('input');
	input.setAttribute('type', 'text');
	input.setAttribute('value', '');
	input.setAttribute('placeholder', 'Введіть діаметр кола');

	circleButton.onclick = () => {
		circleButton.remove();
		divResult.append(buttonPaint);
		divResult.append(input);
	}

	buttonPaint.onclick = () => {
		const diameterCircle = input.value + 'px';
		buttonPaint.remove();
		input.remove();

		let i = 100;
		while (i != 0) {
			const circle = document.createElement('div');
			circle.setAttribute('class', 'circle');
			circle.style.background = `rgb(${(Math.random() * 255).toFixed(0)}, ${(Math.random() * 255).toFixed(0)}, ${(Math.random() * 255).toFixed(0)})`;
			circle.style.height = diameterCircle;
			circle.style.width = diameterCircle;
			divResult.append(circle);
			i--;
		};
	};

	divResult.addEventListener('click', (event) => {
		if (event.target.classList.contains('circle')) {
			event.target.remove();
		};
	})
};
